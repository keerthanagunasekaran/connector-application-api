# the first stage of our build will use a maven 3.6.1 parent image
FROM maven:3.8.1-openjdk-16 AS MAVEN_BUILD

ARG IDHUB_REPO_USER
ARG IDHUB_REPO_PASSWORD

# copy the pom and src code to the container
COPY ./ ./

RUN user="$(echo $IDHUB_REPO_USER)" && echo $user

RUN mkdir -p /root/.m2 \
    && mkdir /root/.m2/repository

RUN file="$(ls)" && echo ${file}
# package our application code
RUN mvn -s mavenSettings.xml -e clean package

# the second stage of our build will use open jdk 8 on alpine 3.9
FROM openjdk:16-jre

# copy only the artifacts we need from the first stage and discard the rest
COPY --from=MAVEN_BUILD target/'IDHub COnnector Application - 2.0.0.jar' /connector.jar

# set the startup command to execute the jar
CMD ["java", "-jar", "/connector.jar"]