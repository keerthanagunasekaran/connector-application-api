# Connector Application Java

## Building the Application

run ```mvn package```

## Configure the Application

### Java Libraries

Java librarie(s) (```.jar``` file(s)) containing a ```CommonConnectorService``` and one or more
```ScimResourceService``` placed in ```./lib/```. A ***Splice*** is a Java library containing both the
**Connector Service** and the **Resource Service(s)**

### External Configuration

External configurations are contained in and loaded from application.properties or application.yml by Spring-Boot.
See [Spring-Boot External Configuration](https://docs.spring.io/spring-boot/docs/1.0.x/reference/html/boot-features-external-config.html)
for more information.

This file must contain the following fields

```yaml
Spring:
  security:
    oauth2:
      resourceserver:
        jwt:
          jwk-set-uri:
server:
  port:
idhub:
  hostname:
  realm:
  client-id:
  accessToken:
  refreshToken:
```

Where

- `jwk-set-uri` is the uri of the JWK set endpoint
- `hostname` the hostname of the authorization server.
- `realm` is the name of the realm the connector belongs to
- `client-id` the client-id to be used for authentication
- `accessToken` is the access token
- `refreshToken` is the refresh token

### Test Configuration

An application-test.yaml or application-test.properties must be specified in `src/test/resources/` in order to 
configure authentication for use during testing.

```yaml
idhub:
  secret:
  test:
```

Where

- `secret` is the secret used to request a new access token from the client realm.
- `test` is a boolean value that must be set to true in order for the connector use the secret to aquire a new token

## Run the application

### Locally

```bash
java -jar 'IDHub Connector Application - 2.0.0.jar'
```

