package com.sath.idhub.connector.application;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sath.idhub.connector.datatypes.CallbackRegistration;
import com.sath.idhub.connector.datatypes.scim.*;
import com.sath.idhub.connector.interfaces.ScimResourceService;
import com.sath.idhub.connector.splice.development.DevelopmentResourceService;
import com.sath.idhub.connector.utility.ConnectorCallbackUtility;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@ContextConfiguration
@TestExecutionListeners(mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ConnectorApplicationTest
{
	@Autowired
	private WebApplicationContext wac;

	private static final Logger logger = LoggerFactory.getLogger(ConnectorApplicationTest.class);

	private static final ObjectMapper objectMapper = new ObjectMapper();

	private CallbackRegistration callbackRegistrationA;
	private CallbackRegistration callbackRegistrationB;
	private CallbackRegistration callbackRegistrationC;
	private CallbackRegistration callbackRegistrationD;

	@TestConfiguration
	static class TestConfigurations {}

	@Autowired
	ConnectorCallbackUtility connectorCallbackUtility;

	private MockMvc mockMvc;

	private <T> T logAndReturnResponse(MvcResult result, TypeReference<T> typeReference)
	{
		if (result.getResponse().getStatus() == 200)
		{
			T response = Assertions.assertDoesNotThrow(
				()->objectMapper.readValue(result.getResponse().getContentAsString(), typeReference)
			);

			logger.info(response.toString());

			return response;
		}
		else
		{
			ScimError scimError = Assertions.assertDoesNotThrow(
				()->objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {})
			);
			logger.error("Expected HTTP Status 200 but received HTTP Status " + result.getResponse().getStatus());

			if (scimError != null)
			{
				logger.error(scimError.toString());
				logger.error(scimError.detail);
			}

			return null;
		}
	}

	private List<CallbackRegistration> getCallbackRegistrationList(ListResponse listResponse)
	{
		List<CallbackRegistration> callbackRegistrationList = new ArrayList<>();

		for (Object resource : listResponse.resources)
		{
			callbackRegistrationList.add(objectMapper.convertValue(resource, new TypeReference<>() {}));
		}
		return callbackRegistrationList;
	}

	@BeforeAll
	public void exchangeTokens()
	{
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
			.apply(springSecurity())
			.build();

		Assertions.assertDoesNotThrow(
			()-> connectorCallbackUtility.conductTokenExchange()
		);
	}

	@Test
	@Order(1)
	@DisplayName("GET /actuator/health")
	public void testHealth()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/actuator/health")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andReturn()
		);

		if (result.getResponse().getStatus() == 200)
		{
			JsonNode root = Assertions.assertDoesNotThrow(
				()-> objectMapper.readTree(result.getResponse().getContentAsString())
			);

			Assertions.assertDoesNotThrow(
				()->logger.info(
					objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(root)
				)
			);

			JsonNode node = root.path("status");
			Assertions.assertFalse(node.isMissingNode());
			Assertions.assertEquals("UP", node.asText());

			node = root.path("components").path("scimResourceServices");
			Assertions.assertFalse(node.isMissingNode());

			node = root.path("components").path("scimResourceServices").path("details");
			Assertions.assertFalse(node.isMissingNode());

			node = root.path("components").path("scimResourceServices").path("details").path("CallbackRegistration");
			Assertions.assertFalse(node.isMissingNode());

			node = root.path("components").path("scimResourceServices").path("details").path("CallbackRegistration")
				.path("status");
			Assertions.assertFalse(node.isMissingNode());
			Assertions.assertEquals("UP", node.asText());

			node = root.path("components").path("scimResourceServices").path("details").path("Dev");
			Assertions.assertFalse(node.isMissingNode());

			node = root.path("components").path("scimResourceServices").path("details").path("Dev").path("status");
			Assertions.assertFalse(node.isMissingNode());
			Assertions.assertEquals("UP", node.asText());
		}
		else
		{
			Assertions.fail();
		}
	}

	@Test
	@Order(1)
	@DisplayName("GET /scim/v2/ServiceProviderConfig")
	public void testServiceProviderConfig()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/ServiceProviderConfig")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andReturn()
		);

		ServiceProviderConfig serviceProviderConfig = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(serviceProviderConfig);

		// TODO: evaluate moving this stuff to splice information
		ServiceProviderConfig expected = new ServiceProviderConfig.Builder()
			.setBulk(new ServiceProviderConfig.Bulk(false, 1, 1048576))
			// TODO Write test to confirm enforcement of maxResults, requesting a count exceeding this should throw bad
			//  request
			.setFilter(new ServiceProviderConfig.Filter(true, 1000))
			.setChangePassword(new ServiceProviderConfig.ChangePassword(false))
			.setSort(new ServiceProviderConfig.Sort(false))
			.setEtag(new ServiceProviderConfig.Etag(false))
			.setAuthenticationSchemes(
				new ServiceProviderConfig.AuthenticationSchemes(
					"oauth2", "OAuth 2.0", "OAuth 2.0", null, null
				)
			)
			.create();

		Assertions.assertEquals(expected, serviceProviderConfig);
	}

	@Test
	@Order(1)
	@DisplayName("GET /scim/v2/ResourceTypes")
	public void testResourceTypes1()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/ResourceTypes")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:ListResponse")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalResults").value(2))
				.andExpect(MockMvcResultMatchers.jsonPath("$.startIndex").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.itemsPerPage").doesNotExist())
				// Development
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:ResourceType"
					)
				)
				.andExpect(
					MockMvcResultMatchers.jsonPath("$.resources[0].id").value("Development")
				)
				.andExpect(
					MockMvcResultMatchers.jsonPath("$.resources[0].name").value("Development")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].description")
					.value("This resource outputs to the logs and returns your query to you in some form " +
					       "depending on the normal format of the endpoint (Resource or ListResponse).")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].endpoint")
					.value("Developments")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].schema")
					.value("urn:sath:params:scim:api:core:1.0:Dev")
				)
				// CallbackRegistration
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:ResourceType"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].id")
					.value("CallbackRegistration")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].name").value("CallbackRegistration")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].description")
					.value("This resource is used to register conditional callbacks with SCIM Resource " +
					       "Service's that support callback registration.")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].endpoint")
					.value("CallbackRegistrations")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].schema")
					.value("urn:sath:params:scim:api:message:1.0:CallbackRegistration")
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(1)
	@DisplayName("GET /scim/v2/ResourceTypes/{id}")
	public void testResourceType2()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/ResourceTypes/{id}", "Development")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:ResourceType")
				)
				// Development
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:ResourceType"
					)
				)
				.andExpect(
					MockMvcResultMatchers.jsonPath("$.id").value("Development")
				)
				.andExpect(
					MockMvcResultMatchers.jsonPath("$.name").value("Development")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.description")
					.value("This resource outputs to the logs and returns your query to you in some form " +
					       "depending on the normal format of the endpoint (Resource or ListResponse).")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.endpoint")
					.value("Developments")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.schema")
					.value("urn:sath:params:scim:api:core:1.0:Dev")
				)
				.andReturn()
		);

		ResourceType resourceType = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(1)
	@DisplayName("GET /scim/v2/Schemas")
	public void testSchemas1()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/Schemas")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:ListResponse")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalResults").value(2))
				.andExpect(MockMvcResultMatchers.jsonPath("$.startIndex").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.itemsPerPage").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:Schema"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].id").value(
					"urn:sath:params:scim:api:core:1.0:Dev"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:Schema"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[1].id").value(
					"urn:sath:params:scim:api:message:1.0:CallbackRegistration"
					)
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(1)
	@DisplayName("GET /scim/v2/Schemas/Dev")
	public void testSchemas2()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/Schemas/Dev")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:Schema")
				)
				.andExpect(
					MockMvcResultMatchers.jsonPath("$.id").value(
						"urn:sath:params:scim:api:core:1.0:Dev"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[0].name").value("externalId")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[1].name").value("id")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].name").value("meta")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[0].name").value("resourceType")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[1].name").value("created")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[2].name").value("lastModified")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[3].name").value("location")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[4].name").value("version")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].name").value("schemas")
				)
				.andReturn()
		);

		Schema ignored = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(1)
	@DisplayName("GET /scim/v2/Schemas/CallbackRegistration")
	public void testSchemas3()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/Schemas/CallbackRegistration")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:schemas:core:2.0:Schema")
				)
				// Common
				.andExpect(
					MockMvcResultMatchers.jsonPath("$.id").value(
						"urn:sath:params:scim:api:message:1.0:CallbackRegistration"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[0].name").value("externalId")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[1].name").value("id")
				)
				// Meta
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].name").value("meta")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[0].name").value("resourceType")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[1].name").value("created")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[2].name").value("lastModified")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[3].name").value("location")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[2].subAttributes[4].name").value("version")
				)
				// ResourceType
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].name").value("resourceType")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].subAttributes[0].name").value("schemas")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].subAttributes[1].name").value("id")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].subAttributes[2].name").value("name")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].subAttributes[3].name").value("description")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].subAttributes[4].name").value("endpoint")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].subAttributes[5].name").value("schema")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[3].subAttributes[6].name").value("schemaExtension")
				)
				// Schemas
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[4].name").value("schemas")
				)
				//Search Request
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].name").value("searchRequest")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[0].name").value("schemas")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[1].name").value("attributes")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[2].name").value("excludedAttributes")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[3].name").value("filter")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[4].name").value("sortBy")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[5].name").value("sortOrder")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[6].name").value("startIndex")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[5].subAttributes[7].name").value("count")
				)
				// URL
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.attributes[6].name").value("urlToBeNotifiedOnChange")
				)
				.andReturn()
		);

		Schema ignored = logAndReturnResponse(result, new TypeReference<>() {});
	}

	// Development Resource Service

	@Test
	@Order(2)
	@DisplayName("GET /scim/v2/Developments")
	public void testDevelopment1()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/Developments/")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:ListResponse")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalResults").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.startIndex").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.itemsPerPage").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:SearchRequest"
					)
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(2)
	@DisplayName("GET /scim/v2/Developments?")
	public void testDevelopment2()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/Developments/")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.param("attributes", "id")
						.param("excludedAttributes", "externalId")
						.param("filter", "name eq \"john\"")
						.param("sortBy", "name")
						.param("sortOrder", "ascending")
						.param("startIndex", "1")
						.param("count", "100")
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:ListResponse")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalResults").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.startIndex").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.itemsPerPage").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:SearchRequest"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].attributes").value("id")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].excludedAttributes").value("externalId")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].filter").value(
					"name eq \"john\"")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].sortBy").value("name")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].sortOrder").value("ascending")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].startIndex").value("1")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].count").value("100")
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(2)
	@DisplayName("GET /scim/v2/Developments/{id}?")
	public void testDevelopment4()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/Developments/{id}", 1)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.param("attributes", "id")
						.param("excludedAttributes", "externalId")
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.queryId").value("1")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.queryAttributes[0]").value("id")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.queryExcludedAttributes[0]").value("externalId")
				)
				.andReturn()
		);

		DevelopmentResourceService.Development development = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(2)
	@DisplayName("GET /scim/v2/Developments/{id}")
	public void testDevelopment3()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/Developments/{id}", 1)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value("1"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name").value("1"))
				.andReturn()
		);

		DevelopmentResourceService.Development development = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(2)
	@DisplayName("POST /scim/v2/Developments")
	public void testDevelopment5()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/Developments")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(
							new DevelopmentResourceService.Development(
								null, null, null, "displayName 1",
								"description 1", null, null, null,
								null
							).toString()
						)
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.schemas").value("urn:sath:params:scim:api:core:1.0:Development")
				)
				.andReturn()
		);

		DevelopmentResourceService.Development development = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(2)
	@DisplayName("POST /scim/v2/Developments/.search")
	public void testDevelopment6()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("name eq john")
			.setSortBy("name")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/Developments/.search")
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:ListResponse")
				)
				.andExpect(MockMvcResultMatchers.jsonPath("$.totalResults").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.startIndex").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.itemsPerPage").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resources[0].schemas").value(
					"urn:ietf:params:scim:api:messages:2.0:SearchRequest"
					)
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].attributes").value("id")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].excludedAttributes").value("externalId")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].filter").value("name eq john")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].sortBy").value("name")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].sortOrder").value("ascending")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].startIndex").value("1")
				)
				.andExpect(MockMvcResultMatchers.jsonPath(
					"$.resources[0].count").value("100")
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(2)
	@DisplayName("PUT /scim/v2/Developments")
	public void testDevelopment7()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->{
				final String id = "id";
				return mockMvc.perform
					(
						MockMvcRequestBuilders.put("/scim/v2/Developments/{id}", id)
							.accept(MediaType.APPLICATION_JSON)
							.header("tenant", connectorCallbackUtility.getRealm())
							.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
							.contentType(MediaType.APPLICATION_JSON)
							.content(
								new DevelopmentResourceService.Development(
									id, null, null, "displayName 2",
									"description 2", null, null,
									null, null
								).toString()
							)
					)
					.andExpect(MockMvcResultMatchers.status().isOk())
					.andExpect(MockMvcResultMatchers.jsonPath("$.queryId").value(id))
					.andExpect(MockMvcResultMatchers.jsonPath("$.queryDisplayName")
						.value("displayName 2")
					)
					.andExpect(MockMvcResultMatchers.jsonPath("$.queryDescription")
						.value("description 2")
					)
					.andReturn();
			}
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});
	}

	@Test
	@Order(2)
	@DisplayName("PATCH /scim/v2/Developments")
	public void testDevelopment8()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.patch("/scim/v2/Developments/")
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.content(
							"""
							{
							    "schemas": [
							        "urn:ietf:params:scim:api:messages:2.0:PatchOp"
							    ],
							    "Operations": [
							        {
							            "op": "replace",
							            "path": "externalId",
							            "value": [
							                {
							                    "display": "Babs Jensen"
							                }
							            ]
							        }
							    ]
							}"""
						)
				)
				.andExpect(MockMvcResultMatchers.status().isMethodNotAllowed())
				.andReturn()
		);
	}

	@Test
	@Order(3)
	@DisplayName("DEL /scim/v2/Developments")
	public void testDevelopment9()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.delete("/scim/v2/Developments/{id}", "Development")
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(MockMvcResultMatchers.status().isNoContent())
				.andReturn()
		);
	}

	// Callback Registration Resource Service

	@Test
	@Order(4)
	@DisplayName("POST /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration1a()
	{
		ResourceType resourceType = new ResourceType.Builder()
			.setId("CallbackRegistration")
			.setName("CallbackRegistration")
			.setEndpoint("CallbackRegistrations")
			.setSchema(CallbackRegistration.schema.id)
			.setDescription(
				"This resource is used to register conditional callbacks with SCIM " +
				"Resource Service's that " +
				"support callback registration.")
			.create();

		SearchRequest searchRequest = new SearchRequest.Builder()
			.setCount(10)
			.setExcludedAttributes(List.of("externalId"))
			.create();

		String displayName = "a displayName";
		String description = "a description keyword";

		URL urlToBeNotifiedOnChange = Assertions.assertDoesNotThrow(()->new URL("https://127.0.0.1:8805/scim/v2/Developments"));

		String content = Assertions.assertDoesNotThrow(
			()->new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(
				new CallbackRegistration(
					displayName,
					description,
					resourceType,
					searchRequest,
					urlToBeNotifiedOnChange
				)
			)
		);

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(content)
				)
				.andReturn()
		);

		callbackRegistrationA = logAndReturnResponse(result, new TypeReference<>() {});

		if (callbackRegistrationA != null)
		{
			Assertions.assertEquals(description, callbackRegistrationA.description);
			Assertions.assertEquals(displayName, callbackRegistrationA.displayName);
			Assertions.assertEquals(resourceType, callbackRegistrationA.resourceType);
			Assertions.assertEquals(searchRequest, callbackRegistrationA.searchRequest);
			Assertions.assertEquals(urlToBeNotifiedOnChange, callbackRegistrationA.urlToBeNotifiedOnChange);
			Assertions.assertEquals(CallbackRegistration.schema.id, callbackRegistrationA.schemas.get(0));
			Assertions.assertNotNull(callbackRegistrationA.id);
			Assertions.assertNotNull(callbackRegistrationA.meta);
		}
	}

	@Test
	@Order(4)
	@DisplayName("POST /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration1b()
	{
		ResourceType resourceType = new ResourceType.Builder()
			.setId("CallbackRegistration")
			.setName("CallbackRegistration")
			.setEndpoint("CallbackRegistrations")
			.setSchema(CallbackRegistration.schema.id)
			.setDescription(
				"This resource is used to register conditional callbacks with SCIM " +
				"Resource Service's that " +
				"support callback registration.")
			.create();

		SearchRequest searchRequest = new SearchRequest.Builder().setCount(21).create();

		String displayName = "a displayName 2";
		String description = "b description 2";

		URL urlToBeNotifiedOnChange = Assertions.assertDoesNotThrow(()->new URL("https://127.0.0.1"));

		String content = Assertions.assertDoesNotThrow(
			()->new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(
				new CallbackRegistration(
					displayName,
					description,
					resourceType,
					searchRequest,
					urlToBeNotifiedOnChange
				)
			)
		);

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(content)
				)
				.andReturn()
		);

		callbackRegistrationB = logAndReturnResponse(result, new TypeReference<>() {});

		if (callbackRegistrationB != null)
		{
			Assertions.assertEquals(description, callbackRegistrationB.description);
			Assertions.assertEquals(displayName, callbackRegistrationB.displayName);
			Assertions.assertEquals(resourceType, callbackRegistrationB.resourceType);
			Assertions.assertEquals(searchRequest, callbackRegistrationB.searchRequest);
			Assertions.assertEquals(urlToBeNotifiedOnChange, callbackRegistrationB.urlToBeNotifiedOnChange);
			Assertions.assertEquals(CallbackRegistration.schema.id, callbackRegistrationB.schemas.get(0));
			Assertions.assertNotNull(callbackRegistrationB.id);
			Assertions.assertNotNull(callbackRegistrationB.meta);
		}
	}

	@Test
	@Order(4)
	@DisplayName("POST /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration1c()
	{
		ResourceType resourceType = new ResourceType.Builder()
			.setId("CallbackRegistration")
			.setName("CallbackRegistration")
			.setEndpoint("CallbackRegistrations")
			.setSchema(CallbackRegistration.schema.id)
			.setDescription(
				"This resource is used to register conditional callbacks with SCIM " +
				"Resource Service's that " +
				"support callback registration.")
			.create();

		SearchRequest searchRequest = new SearchRequest.Builder().setCount(32).create();

		String displayName = "a displayName 3";
		String description = "c description 3";

		URL urlToBeNotifiedOnChange = Assertions.assertDoesNotThrow(()->new URL("https://127.0.0.1"));

		String content = Assertions.assertDoesNotThrow(
			()->new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(
				new CallbackRegistration(
					displayName,
					description,
					resourceType,
					searchRequest,
					urlToBeNotifiedOnChange
				)
			)
		);

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(content)
				)
				.andReturn()
		);

		callbackRegistrationC = logAndReturnResponse(result, new TypeReference<>() {});

		if (callbackRegistrationC != null)
		{
			Assertions.assertEquals(description, callbackRegistrationC.description);
			Assertions.assertEquals(displayName, callbackRegistrationC.displayName);
			Assertions.assertEquals(resourceType, callbackRegistrationC.resourceType);
			Assertions.assertEquals(searchRequest, callbackRegistrationC.searchRequest);
			Assertions.assertEquals(urlToBeNotifiedOnChange, callbackRegistrationC.urlToBeNotifiedOnChange);
			Assertions.assertEquals(CallbackRegistration.schema.id, callbackRegistrationC.schemas.get(0));
			Assertions.assertNotNull(callbackRegistrationC.id);
			Assertions.assertNotNull(callbackRegistrationC.meta);
		}
	}

	@Test
	@Order(4)
	@DisplayName("POST /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration1d()
	{
		ResourceType resourceType = new ResourceType.Builder()
			.setId("CallbackRegistration")
			.setName("CallbackRegistration")
			.setEndpoint("CallbackRegistrations")
			.setSchema(CallbackRegistration.schema.id)
			.setDescription(
				"This resource is used to register conditional callbacks with SCIM " +
				"Resource Service's that " +
				"support callback registration.")
			.create();

		SearchRequest searchRequest = new SearchRequest.Builder().setCount(43).create();

		String displayName = "displayName 4";
		String description = "d keyword description 4";

		URL urlToBeNotifiedOnChange = Assertions.assertDoesNotThrow(()->new URL("https://127.0.0.1"));

		String content = Assertions.assertDoesNotThrow(
			()->new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(
				new CallbackRegistration(
					displayName,
					description,
					resourceType,
					searchRequest,
					urlToBeNotifiedOnChange
				)
			)
		);

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(content)
				)
				.andReturn()
		);

		callbackRegistrationD = logAndReturnResponse(result, new TypeReference<>() {});

		if (callbackRegistrationD != null)
		{
			Assertions.assertEquals(description, callbackRegistrationD.description);
			Assertions.assertEquals(displayName, callbackRegistrationD.displayName);
			Assertions.assertEquals(resourceType, callbackRegistrationD.resourceType);
			Assertions.assertEquals(searchRequest, callbackRegistrationD.searchRequest);
			Assertions.assertEquals(urlToBeNotifiedOnChange, callbackRegistrationD.urlToBeNotifiedOnChange);
			Assertions.assertEquals(CallbackRegistration.schema.id, callbackRegistrationD.schemas.get(0));
			Assertions.assertNotNull(callbackRegistrationD.id);
			Assertions.assertNotNull(callbackRegistrationD.meta);
		}
	}

	@Test
	@Order(5)
	@DisplayName("GET /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration2()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/CallbackRegistrations/")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertEquals(
			"urn:ietf:params:scim:api:messages:2.0:ListResponse", listResponse.schemas.get(0)
		);
		Assertions.assertEquals(4, listResponse.totalResults);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));
	}

	@Test
	@Order(5)
	@DisplayName("GET /scim/v2/CallbackRegistrations?")
	public void testCallbackRegistration3()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/CallbackRegistrations/")
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.param("attributes", "id")
						.param("excludedAttributes", "externalId")
						.param(
							"filter",
							"id pr and not (displayName eq \"seven\" or (meta.created gt " +
							ScimResourceService.dateTimeFormat.format(ZonedDateTime.now()) + "))"
						)
						.param("sortBy", "displayName")
						.param("sortOrder", "ascending")
						.param("startIndex", "1")
						.param("count", "100")
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertEquals(4, listResponse.totalResults);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));
	}

	@Test
	@Order(6)
	@DisplayName("GET /scim/v2/CallbackRegistrations/{id}")
	public void testCallbackRegistration4()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/CallbackRegistrations/{id}", callbackRegistrationC.id)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
				)
				.andReturn()
		);

		CallbackRegistration callbackRegistration = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(callbackRegistration);
		Assertions.assertEquals(this.callbackRegistrationC, callbackRegistration);
	}

	@Test
	@Order(6)
	@DisplayName("GET /scim/v2/CallbackRegistrations/{id}?excludedAttribute=")
	public void testCallbackRegistration5()
	{
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.get("/scim/v2/CallbackRegistrations/{id}", callbackRegistrationC.id)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.param(
							"excludedAttributes",
							"externalId", "resourceType", "searchRequest", "urlToBeNotified"
						)
				)
				.andReturn()
		);

		CallbackRegistration callbackRegistration = logAndReturnResponse(result, new TypeReference<>() {});
		Assertions.assertNotNull(callbackRegistration);
		@SuppressWarnings("deprecation")
		CallbackRegistration answer = new CallbackRegistration(
			this.callbackRegistrationC.id,
			null,
			this.callbackRegistrationC.meta,
			this.callbackRegistrationC.displayName,
			this.callbackRegistrationC.description,
			this.callbackRegistrationC.onChange,
			null,
			null,
			null
		);

		Assertions.assertEquals(answer, callbackRegistration);
	}

	// String Filters

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search displayName eq")
	public void testStringEquals()
	{

		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName eq \"" + callbackRegistrationA.displayName + "\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search displayName ne")
	public void testStringNotEquals()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName ne \"" + callbackRegistrationA.displayName + "\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search displayName sw")
	public void testStringStartsWith()
	{
		SearchRequest searchRequest1 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName sw \"a displayName\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest1.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest1.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName sw \"not a display name\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search displayName ew")
	public void testStringEndsWith()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName sw \"a displayName\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName ew \"not a display name\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search description co")
	public void testStringContains()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description co \"keyword\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName ew \"not a real display name\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search description lt")
	public void testStringLessThan()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description lt \"" + callbackRegistrationC.description + "\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName lt \"AAAAAAAAAAAAAAAAAAAA\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search description le")
	public void testStringLessThanOrEqual()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description le \"" + callbackRegistrationC.description + "\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName le \"AAAAAAAAAA\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search description gt")
	public void testStringGreaterThan()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description gt \"" + callbackRegistrationB.description + "\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("displayName gt \"zzzzzzzzzzzzzzzzzzzzzzzzzzzzz\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(7)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search description ge")
	public void testStringGreaterThanOrEqual()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description ge \"" + callbackRegistrationB.description + "\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description ge \"zzzzzzzzzzzzzzzzzzzzzzzzzzzzz\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	// Date Time Filters

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created eq")
	public void testDatetimeEquals()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created eq " + callbackRegistrationC.meta.created.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created eq " + ZonedDateTime.now().format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created ne")
	public void testDatetimeNotEquals()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created ne " + callbackRegistrationC.meta.created.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created lt")
	public void testDatetimeLessThan()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created lt " +
				callbackRegistrationC.meta.created.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created lt " +
				callbackRegistrationA.meta.created.minus(1, ChronoUnit.SECONDS)
					.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created le")
	public void testDatetimeLessThanOrEqual()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created le " +
				callbackRegistrationC.meta.created.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created le " +
				callbackRegistrationA.meta.created.minus(1, ChronoUnit.SECONDS)
					.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created gt")
	public void testDatetimeGreaterThan()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created gt " +
				callbackRegistrationB.meta.created.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created gt " +
				callbackRegistrationD.meta.created.plus(1, ChronoUnit.SECONDS)
					.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created ge")
	public void testDatetimeGreaterThanOrEqual()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created ge " +
				callbackRegistrationB.meta.created.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created ge " +
				callbackRegistrationD.meta.created.plus(1, ChronoUnit.SECONDS)
					.format(ScimResourceService.dateTimeFormat)
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created sw")
	public void testDatetimeStartsWith()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created sw \"" +
				callbackRegistrationB.meta.created.format(ScimResourceService.dateTimeFormat).substring(0, 23) +
				"\""
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created sw \"" +
				ZonedDateTime.now().format(ScimResourceService.dateTimeFormat).substring(0, 23) +
				"\""
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created ew")
	public void testDatetimeEndsWith()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created ew \"" +
				callbackRegistrationB.meta.created.format(ScimResourceService.dateTimeFormat).substring(10) + "\""
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created ew \"" +
				ZonedDateTime.now().format(ScimResourceService.dateTimeFormat).substring(10) +
				"\""
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(8)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search meta.created co")
	public void testDatetimeContains()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created co \"" +
				callbackRegistrationB.meta.created.format(ScimResourceService.dateTimeFormat).substring(10, 23) + "\""
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"meta.created co \"" +
				ZonedDateTime.now().format(ScimResourceService.dateTimeFormat).substring(10) +
				"\""
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	// Number Filters

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count eq")
	public void testNumberEquals()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count eq 10")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count eq 4")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count ne")
	public void testNumberNotEquals()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"searchRequest.count ne 10"
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count lt")
	public void testNumberLessThan()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter(
				"searchRequest.count lt 30"
			)
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count lt 0")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count le")
	public void testNumberLessThanOrEqual()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count le 33")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count le 0")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count gt")
	public void testNumberGreaterThan()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count gt 22")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count gt 999999")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count ge")
	public void testNumberGreaterThanOrEqual()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count ge 20")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(3, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationC));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationD));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count ge 999999")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count sw")
	public void testNumberStartsWith()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count sw 2")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count sw 999999")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count ew")
	public void testNumberEndsWith()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count ew 1")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count ew 999999")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(9)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count co")
	public void testNumberContains()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count co 1")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationB));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.count co 999999")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	// List<String>

	@Test
	@Order(10)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search searchRequest.count co")
	public void testStringListContains()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.excludedAttributes co \"externalId\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(1, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());
		Assertions.assertTrue(callbackRegistrationList.contains(callbackRegistrationA));

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("searchRequest.attributes co \"Not an Attribute\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(100)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(11)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search count w/ filter")
	public void testCountWithFilter()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description ge \"" + callbackRegistrationB.description + "\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(2)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description ge \"zzzzzzzzzzzzzzzzzzzzzzzzzzzzz\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(0)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(11)
	@DisplayName("POST /scim/v2/CallbackRegistrations/.search count")
	public void testCount()
	{
		SearchRequest searchRequest = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(2)
			.create();

		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest.toString())
				)
				.andReturn()
		);

		ListResponse listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		List<CallbackRegistration> callbackRegistrationList = getCallbackRegistrationList(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(2, listResponse.totalResults);
		Assertions.assertEquals(listResponse.totalResults, callbackRegistrationList.size());

		// No Results

		SearchRequest searchRequest2 = new SearchRequest.Builder()
			.addAttribute("id")
			.addExcludedAttribute("externalId")
			.setFilter("description ge \"zzzzzzzzzzzzzzzzzzzzzzzzzzzzz\"")
			.setSortBy("id")
			.setSortOrder("ascending")
			.setStartIndex(1)
			.setCount(0)
			.create();

		result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.post("/scim/v2/CallbackRegistrations/.search")
						.header(
							"content-length",
							searchRequest2.toString().getBytes(StandardCharsets.UTF_8).length
						)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(searchRequest2.toString())
				)
				.andReturn()
		);

		listResponse = logAndReturnResponse(result, new TypeReference<>() {});

		Assertions.assertNotNull(listResponse);

		Assertions.assertNull(listResponse.itemsPerPage);
		Assertions.assertEquals(1, listResponse.startIndex);
		Assertions.assertEquals(0, listResponse.totalResults);
	}

	@Test
	@Order(12)
	@DisplayName("PUT /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration8()
	{
		@SuppressWarnings("unused")
		MvcResult ignored = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.put("/scim/v2/CallbackRegistrations/{id}", callbackRegistrationC.id)
						.accept(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.contentType(MediaType.APPLICATION_JSON)
						.content(
							new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(
								new CallbackRegistration(
									callbackRegistrationC,
									"new id"
								)
							)
						)
				)
				.andExpect(MockMvcResultMatchers.status().isNotImplemented())
				.andReturn()
		);
	}

	@Test
	@Order(12)
	@DisplayName("PATCH /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration9()
	{
		@SuppressWarnings("unused")
		MvcResult ignored = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.patch("/scim/v2/CallbackRegistrations/")
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.content(
							"""
							{
							    "schemas": [
							        "urn:ietf:params:scim:api:messages:2.0:PatchOp"
							    ],
							    "Operations": [
							        {
							            "op": "replace",
							            "path": "externalId",
							            "value": [
							                {
							                    "display": "Babs Jensen"
							                }
							            ]
							        }
							    ]
							}"""
						)
				)
				.andExpect(MockMvcResultMatchers.status().isMethodNotAllowed())
				.andReturn()
		);
	}

	@Test
	@Order(13)
	@DisplayName("DEL /scim/v2/CallbackRegistrations")
	public void testCallbackRegistration10()
	{
		@SuppressWarnings("unused")
		MvcResult result = Assertions.assertDoesNotThrow(
			()->mockMvc.perform
				(
					MockMvcRequestBuilders.delete("/scim/v2/CallbackRegistrations/{id}", callbackRegistrationC.id)
						.contentType(MediaType.APPLICATION_JSON)
						.header("tenant", connectorCallbackUtility.getRealm())
						.header("Authorization", "Bearer " + connectorCallbackUtility.getExchangedToken())
						.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(MockMvcResultMatchers.status().isNoContent())
				.andReturn()
		);
	}
}