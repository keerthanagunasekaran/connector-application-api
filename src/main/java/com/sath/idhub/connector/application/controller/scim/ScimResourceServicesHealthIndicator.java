package com.sath.idhub.connector.application.controller.scim;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sath.idhub.connector.exceptions.scim.InternalServerErrorException;
import com.sath.idhub.connector.exceptions.scim.NotImplementedException;
import com.sath.idhub.connector.interfaces.ScimResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

/**
 * A SCIM Resource Health Indicator which implements <code>HealthIndicator</code>
 */
@Component
public class ScimResourceServicesHealthIndicator implements HealthIndicator
{
   private static final ObjectMapper objectMapper = new ObjectMapper();
   private final ScimResourceService[] scimResourceServices;

   ScimResourceServicesHealthIndicator(@Autowired ScimResourceService[] scimResourceServices)
   {
      this.scimResourceServices = scimResourceServices;
   }

   @Override
   public Health health()
   {
      Health.Builder healthBuilder = new Health.Builder();

      enum WorstHealth
      {
         UP (1),
         DOWN(2),
         UNKNOWN(3),
         OUT_OF_SERVICE(4);

         private final int severity;

         WorstHealth(int severity)
         {
            this.severity = severity;
         }

         WorstHealth(Status status)
         {
            if (Status.UP.equals(status))
            {
               this.severity = 1;
            }
            else if (Status.DOWN.equals(status))
            {
               this.severity = 2;
            }
            else if (Status.UNKNOWN.equals(status))
            {
               this.severity = 3;
            }
            else if (Status.OUT_OF_SERVICE.equals(status))
            {
               this.severity = 4;
            }
            else
            {
               throw new IllegalStateException("Unexpected value: " + status);
            }
         }

         public int getSeverity()
         {
            return severity;
         }
      }

      WorstHealth worstHealth = WorstHealth.UP;

      for (ScimResourceService scimResourceService : scimResourceServices)
      {
         String resourceName;
         try
         {
            resourceName = scimResourceService.getScimResourceServiceInformation().resourceName;
         }
         catch (InternalServerErrorException | NotImplementedException e)
         {
            continue;
         }

         try
         {
            Health health = scimResourceService.getHealth();

            healthBuilder.withDetail(resourceName, health);

            WorstHealth status = switch (health.getStatus().toString())
            {
               case "UP" -> WorstHealth.UP;
               case "DOWN" -> WorstHealth.DOWN;
               case "UKNOWN" -> WorstHealth.UNKNOWN;
               case "OUT_OF_SERVICE" -> WorstHealth.OUT_OF_SERVICE;
               default -> throw new IllegalStateException("Unexpected value: " + health.getStatus());
            };

            if (status.getSeverity() > worstHealth.getSeverity())
            {
               healthBuilder.withDetail(resourceName, new Health.Builder().unknown().build());
            }
         }
         // Health Unknown
         catch (InternalServerErrorException e)
         {
            worstHealth = WorstHealth.OUT_OF_SERVICE;
            healthBuilder.withDetail(resourceName, new Health.Builder().outOfService().build());
         }
         catch (NotImplementedException e)
         {
            if (WorstHealth.UNKNOWN.getSeverity() > worstHealth.getSeverity())
            {
               healthBuilder.withDetail(resourceName, new Health.Builder().unknown().build());
            }
         }
      }

      return switch (worstHealth.getSeverity())
         {
            case 1 -> healthBuilder.up().build();
            case 2 -> healthBuilder.down().build();
            case 4 -> healthBuilder.outOfService().build();
            default -> healthBuilder.unknown().build();
         };
   }
}