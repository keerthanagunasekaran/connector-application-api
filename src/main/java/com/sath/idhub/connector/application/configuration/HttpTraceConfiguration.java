package com.sath.idhub.connector.application.configuration;

import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Http Trace Configuration Class
 */
@Configuration
public class HttpTraceConfiguration
{
   /**
    * @return a new HttpTraceRepository
    */
   @Bean
   public HttpTraceRepository httpTraceRepository()
   {
      return new InMemoryHttpTraceRepository();
   }
}
