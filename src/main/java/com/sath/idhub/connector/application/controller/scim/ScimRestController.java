package com.sath.idhub.connector.application.controller.scim;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.sath.idhub.connector.datatypes.*;
import com.sath.idhub.connector.datatypes.scim.*;
import com.sath.idhub.connector.exceptions.scim.*;
import com.sath.idhub.connector.interfaces.*;
import com.sath.idhub.connector.utility.ConnectorCallbackUtility;

import com.sath.idhub.connector.utility.exception.CallbackFailedException;
import com.sath.idhub.connector.utility.exception.TokenExchangeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.*;

// TODO: Add should expose variable to spi and use it.
// TODO: update splice parent project

/**
 * A Rest Controller that provides access to an underlying target system's resources.
 */
@RestController
@EnableAsync(proxyTargetClass = true)
@RequestMapping(value = "/scim/v2/", produces = "application/json")
public class ScimRestController
{
	private static final Logger       logger       = LoggerFactory.getLogger(ScimRestController.class);
	private static final ObjectMapper objectMapper = new ObjectMapper();

	private final Map<String, Schema>              schemas              = new HashMap<>();
	private final Map<String, ResourceType>        resourceTypes        = new HashMap<>();
	private final Map<String, ScimResourceService> scimResourceServices = new HashMap<>();

	private final ListResponse             schemasListResponse;
	private final ListResponse             resourceTypesListResponse;
	private final ServiceProviderConfig    serviceProviderConfig;
	private final CommonConnectorService   commonConnectorService;
	private final HttpHeaders              scimJsonHttpHeader;
	private final ConnectorCallbackUtility connectorCallbackUtility;

	private void checkContentLength(int contentLength)
		throws PayloadTooLargeException
	{
		if (contentLength > serviceProviderConfig.bulk.maxPayloadSize)
		{
			throw new PayloadTooLargeException(contentLength, serviceProviderConfig.bulk.maxPayloadSize);
		}
	}

	/**
	 * This method takes the executable code in provided in callable and calls it and returns an appropriate scim error
	 * message based on the scim exception throw. If an exception that doesn't extend ScimException is thrown it gets
	 * wrapped in scim internal sever error.
	 *
	 * @param callable a callable to execute. If it throws errors they will be caught and the wrapper and returned as a
	 *                 SCIM error.
	 * @return A response entity containing the result of the callable or a scim exception.
	 */
	private ResponseEntity<Object> returnExceptionsAsScimErrors(Callable<ResponseEntity<Object>> callable)
	{
		try
		{
			return callable.call();
		}
		catch (ScimException e)
		{
			return ResponseEntity
				.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}
		catch (Exception e)
		{
			InternalServerErrorException internalServerErrorException = new InternalServerErrorException(e);

			return ResponseEntity
				.status(internalServerErrorException.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(internalServerErrorException);
		}
	}

	// FixMe: Doesn't return appropriate scim errors when it fails authorization.
	//      401 Unauthorized -- Unimplemented
	//      403 Forbidden -- Implemented Untested
	//      All need to be handled and return appropriate scim errors.

	/**
	 * @param commonConnectorService    The common connector service to be used by this
	 * @param scimResourceServices      The {@link ScimResourceService}s to be exposed by this rest controller
	 * @param connectorCallbackUtility  The {@link ConnectorCallbackUtility} to be used by this rest controller
	 * @throws InternalServerErrorException when no valid <code>ScimResourceServices</code> can be exposed
	 */
	public ScimRestController(
		@Autowired(required = false) CommonConnectorService commonConnectorService,
		@Autowired ScimResourceService[] scimResourceServices,
		@Autowired ConnectorCallbackUtility connectorCallbackUtility
	)
		throws InternalServerErrorException
	{
		this.connectorCallbackUtility = connectorCallbackUtility;
		this.commonConnectorService   = commonConnectorService;

		boolean supportsSort = registerScimResourceServices(scimResourceServices);

		scimJsonHttpHeader = new HttpHeaders();
		scimJsonHttpHeader.set("Content-Type", "application/scim+json");

		// ToDo: Support Etag at connector level
		ApplicationInformation applicationInformation = this.commonConnectorService != null
		                                                ? this.commonConnectorService.getApplicationInformation()
		                                                : new ApplicationInformation.Builder().setApplicationName(
			                                                "Default Development Connector")
			                                                .setEntitlementAttributeName("entitlements")
			                                                .setMaximumNumberOfOperations(1)
			                                                .setMaximumPayloadSize(1048576) // One MiB
			                                                .create();

		serviceProviderConfig = new ServiceProviderConfig.Builder()
			.setAuthenticationSchemes(
				new ServiceProviderConfig.AuthenticationSchemes(
					"oauth2", "OAuth 2.0", "OAuth 2.0", null, null
				)
			)

			// Maximum number of operation is only enforced on bulk. (And maybe patch) All the other endpoints
			// represent
			// a single operation.
			.setBulk(
				new ServiceProviderConfig.Bulk(
					false,
					applicationInformation.maximumNumberOfOperations,
					applicationInformation.maximumPayloadSize
				)
			)
			.setChangePassword(new ServiceProviderConfig.ChangePassword(false))
			.setDocumentationUri(null)
			// ToDo: Add etag support to Resource Information and use them all &&'d together
			.setEtag(new ServiceProviderConfig.Etag(false))
			.setFilter(new ServiceProviderConfig.Filter(true, 1000))
			.setSort(new ServiceProviderConfig.Sort(supportsSort))
			.create();

		resourceTypesListResponse = new ListResponse.Builder()
			.setResources(List.copyOf(resourceTypes.values()))
			.setStartIndex(1)
			.create();

		schemasListResponse = new ListResponse.Builder()
			.setResources(List.copyOf(schemas.values()))
			.setStartIndex(1)
			.create();
	}

	/**
	 * @param scimResourceServices the {@link ScimResourceService}'s to evaluate for registration.
	 * @return whether or not sort is universally supported
	 * @throws InternalServerErrorException when no {@link ScimResourceService} is registered
	 */
	private boolean registerScimResourceServices(ScimResourceService[] scimResourceServices)
		throws InternalServerErrorException
	{
		boolean supportsSort = true;

		// ToDo: report the bad stuff in the health
		for (ScimResourceService scimResourceService : scimResourceServices)
		{
			ResourceServiceInformation resourceServiceInformation;
			try
			{
				resourceServiceInformation = scimResourceService.getScimResourceServiceInformation();

				// skip this ScimResourceService if it is not meant to be exposed.
				if (!resourceServiceInformation.exposeExternally) continue;

				// ToDo: This kind of stuff should be reported per splice. This could be put into the additional details
				//  of health, or probably some other actuator endpoint.
				if (!resourceServiceInformation.sortSupported)
				{
					supportsSort = false;
				}

				try
				{
					ResourceType resourceType = scimResourceService.getResourceType();

					this.resourceTypes.put(resourceType.id, resourceType);
					this.scimResourceServices.put(resourceType.endpoint, scimResourceService);

					try
					{
						this.schemas.put(resourceServiceInformation.resourceName, scimResourceService.getSchema());
					}
					catch (InternalServerErrorException | NotImplementedException e)
					{
						logger.error(
							"Couldn't retrieve schema for resource: \"" + resourceServiceInformation.resourceName +
							"\"",
							e
						);
					}
				}
				catch (NotImplementedException e)
				{
					logger.error(
						"Couldn't retrieve resourceType for resource: \"" + resourceServiceInformation.resourceName +
						"\"",
						e
					);
				}
			}
			catch (NotImplementedException e)
			{
				logger.error(
					"Unable to retrieve the ScimResourceServiceInformation for " +
					scimResourceService.getClass().toString(),
					e
				);
			}
		}

		if (this.scimResourceServices.isEmpty())
		{
			throw new InternalServerErrorException("No valid SCIM Resource Services provided to expose.");
		}

		return supportsSort;
	}

	/**
	 * @param endpoint                  the endpoint to search asynchronously
	 * @param asynchronousSearchRequest the criteria describing resources to be returned
	 * @return If the connector was able to start the search asynchronously, HTTP Status 200, a SCIM Error Message
	 * 	otherwise.
	 */
	@PostMapping(value = "/{endpoint}/AsyncMethodCall", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Object> getAsyncMethod(
		@PathVariable("endpoint") String endpoint,
		@RequestBody AsynchronousSearchRequest asynchronousSearchRequest
	)
	{
		logger.debug("POST /AsyncMethodCall\nSearch Request: " + asynchronousSearchRequest.toString());

		ScimResourceService scimResourceService = scimResourceServices.get(endpoint);

		if (scimResourceService == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return returnExceptionsAsScimErrors(
			()->{
				Runnable runnable = ()->{
					try
					{
						Future<ListResponse> future =
							scimResourceService.asyncSearchResource(asynchronousSearchRequest);
						ListResponse listResponse = future.get();

						connectorCallbackUtility.sendAsynchronousSearchResults(asynchronousSearchRequest, listResponse);
					}
					catch (
						NotImplementedException | PayloadTooLargeException | InternalServerErrorException |
						ExecutionException | InterruptedException | BadRequestException | TokenExchangeException |
						CallbackFailedException e
					)
					{
						// ToDo: Do something meaningful on failure.
						logger.error(
							"Encountered an unexpected exception trying to forward the results of a asynchronous search"
						);
					}
				};

				Thread thread = new Thread(runnable);

				thread.start();

				return ResponseEntity.status(200)
					.headers(scimJsonHttpHeader)
					.body("{}");
			}
		);
	}

	/**
	 * @return A ListResponse containing the ResourceTypes supported by this connector.
	 */
	@GetMapping("/ResourceTypes")
	@ResponseBody
	public ResponseEntity<ListResponse> getResourceTypes()
	{
		logger.debug("GET /ResourceTypes");

		return ResponseEntity
			.status(200)
			.headers(scimJsonHttpHeader)
			.body(resourceTypesListResponse);
	}

	/**
	 * @param id The id of the <code>ResourceType</code> to be retrieved
	 * @return the <code>ResourceType</code> specified by id
	 */
	@GetMapping("/ResourceTypes/{id}")
	@ResponseBody
	public ResponseEntity<Object> getResourceTypes(@PathVariable("id") @NonNull String id)
	{
		logger.debug("GET /ResourceTypes/" + id);

		ResourceType resourceType = resourceTypes.get(id);

		if (resourceType == null)
		{
			ScimException exception = new NotFoundException();

			return ResponseEntity
				.status(exception.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(exception.toString());
		}
		else
		{
			return ResponseEntity
				.status(200)
				.headers(scimJsonHttpHeader)
				.body(resourceType);
		}
	}

	/**
	 * @return The <code>ServiceProviderConfig</code> for this connector, or an appropriate SCIM Error message
	 */
	@GetMapping("/ServiceProviderConfig")
	@ResponseBody
	public ResponseEntity<ServiceProviderConfig> getServiceProviderConfig()
	{
		logger.debug("GET /ServiceProviderConfig");

		return ResponseEntity
			.status(HttpStatus.OK)
			.headers(scimJsonHttpHeader)
			.body(serviceProviderConfig);
	}

	/**
	 * @return A ListResponse containing the <code>Schemas</code> supported by this connector, or an appropriate SCIM
	 * 	Error message
	 */
	@GetMapping("/Schemas")
	@ResponseBody
	public ResponseEntity<ListResponse> getSchemas()
	{
		logger.debug("GET /Schemas");

		return ResponseEntity
			.status(200)
			.headers(scimJsonHttpHeader)
			.body(schemasListResponse);
	}

	/**
	 * @param id The id of the schema to be returned
	 * @return The <code>Schema</code> referred to by the supplied id, or an appropriate SCIM Error Message
	 */
	@GetMapping("/Schemas/{id}")
	@ResponseBody
	public ResponseEntity<Object> getSchemaById(@PathVariable String id)
	{
		logger.debug("GET /Schemas/" + id);

		Schema schema = schemas.get(id);

		if (schema == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return ResponseEntity.status(200)
			.headers(scimJsonHttpHeader)
			.body(schema);
	}

	/**
	 * @param endpoint           The endpoint of the resource to query, described by the ResourceTypes endpoint
	 * @param attributes         A multi-valued list of strings indicating the names of resource attributes to be
	 *                           returned in the response, overriding the set of attributes that would be returned by
	 *                           default. Attribute names MUST be in standard attribute notation (Section 3.10) form.
	 *                           See Section 3.9 for additional retrieval query parameters. OPTIONAL.
	 * @param excludedAttributes A multi-valued list of strings indicating the names of resource attributes to be
	 *                           removed from the default set of attributes to return.  This parameter SHALL have no
	 *                           effect on attributes whose schema "returned" setting is "always" (see Sections 2.2 and
	 *                           7 of [RFC7643]).  Attribute names MUST be in standard attribute notation (Section
	 *                           3.10)
	 * @param filter             The filter string used to request a subset of resources.  The filter string MUST be a
	 *                           valid filter (Section 3.4.2.2) expression. OPTIONAL.
	 * @param sortBy             A string indicating the attribute whose value SHALL be used to order the returned
	 *                           responses.  The "sortBy" attribute MUST be in standard attribute notation (Section
	 *                           3.10) form.  See Section 3.4.2.3.  OPTIONAL.
	 * @param sortOrder          A string indicating the order in which the "sortBy" parameter is applied.  Allowed
	 *                           values are "ascending" and "descending".  See Section 3.4.2.3.  OPTIONAL.
	 * @param count              An integer indicating the desired maximum number of query results per page.  See
	 *                           Section 3.4.2.4.  OPTIONAL.
	 * @param startIndex         An integer indicating the 1-based index of the first query result.  See Section
	 *                           3.4.2.4.  OPTIONAL.
	 * @return A ListResponse containing all the appropriate resources
	 */
	@GetMapping("/{endpoint}")
	@ResponseBody
	public ResponseEntity<Object> searchResource(
		@PathVariable String endpoint,
		@RequestParam(defaultValue = "") List<String> attributes,
		@RequestParam(defaultValue = "") List<String> excludedAttributes,
		@RequestParam(required = false) String filter,
		@RequestParam(required = false) String sortBy,
		@RequestParam(required = false) String sortOrder,
		@RequestParam(required = false) Integer count,
		@RequestParam(required = false) Integer startIndex
	)
	{
		logger.debug(
			"GET /" + endpoint + "?\n" +
			"Attributes : " + (attributes == null ? "null\n" : attributes + "\n") +
			"Excluded Attributes : " + (excludedAttributes == null ? "null\n" : excludedAttributes + "\n") +
			"Filter : " + (filter == null ? "null\n" : filter + "\n") +
			"Sort By : " + (sortBy == null ? "null\n" : sortBy + "\n") +
			"Sort Order : " + (sortOrder == null ? "null\n" : sortOrder + "\n") +
			"Count : " + (count == null ? "null\n" : count + "\n") +
			"Start Index : " + (startIndex == null ? "null" : startIndex)
		);

		return returnExceptionsAsScimErrors(
			()->{
				SearchRequest searchRequest = new SearchRequest.Builder()
					.setAttributes(attributes)
					.setExcludedAttributes(excludedAttributes)
					.setFilter(filter)
					.setSortBy(sortBy)
					.setSortOrder(sortOrder)
					.setCount(count)
					.setStartIndex(startIndex)
					.create();

				return searchResource(searchRequest.toString().length(), endpoint, searchRequest);
			}
		);
	}

	/**
	 * @param endpoint           The endpoint of the resource to query, described by the ResourceTypes endpoint
	 * @param id                 the id of the resource to be returned
	 * @param attributes         A multi-valued list of strings indicating the names of resource attributes to be
	 *                           returned in the response, overriding the set of attributes that would be returned by
	 *                           default. Attribute names MUST be in standard attribute notation (Section 3.10) form.
	 *                           See Section 3.9 for additional retrieval query parameters.  OPTIONAL.
	 * @param excludedAttributes A multi-valued list of strings indicating the names of resource attributes to be
	 *                           removed from the default set of attributes to return.  This parameter SHALL have no
	 *                           effect on attributes whose schema "returned" setting is "always" (see Sections 2.2 and
	 *                           7 of [RFC7643]).  Attribute names MUST be in standard attribute notation (Section
	 *                           3.10)
	 * @return the resource requested, or an appropriate SCIM Error Message
	 */
	@GetMapping("/{endpoint}/{id}")
	@ResponseBody
	public ResponseEntity<Object> getResourceById(
		@PathVariable String endpoint,
		@PathVariable String id,
		@RequestParam(required = false) Set<String> attributes,
		@RequestParam(required = false) Set<String> excludedAttributes
	)
	{
		logger.debug(
			"GET /" + endpoint + "/" + id + "\n" +
			"Attributes : " + attributes + "\n" +
			"Excluded Attributes : " + excludedAttributes
		);

		ScimResourceService scimResourceService = scimResourceServices.get(endpoint);

		if (scimResourceService == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return returnExceptionsAsScimErrors(
			()->{

				Object resource = scimResourceService.getResource(
					id,
					attributes == null ? new HashSet<>() : attributes,
					excludedAttributes == null ? new HashSet<>() : excludedAttributes
				);

				return ResponseEntity.status(200)
					.headers(scimJsonHttpHeader)
					.body(resource);
			}
		);
	}

	/**
	 * @param contentLength The length of the content in bytes.
	 * @param endpoint      The endpoint of the resource that is to be created, described by the ResourceTypes endpoint
	 * @param scimJson      The SCIM+JSON resource to be constructed
	 * @return the created resource
	 */
	@PostMapping(value = "/{endpoint}", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Object> postResource(
		@RequestHeader("content-length") int contentLength,
		@PathVariable String endpoint, @RequestBody String scimJson
	)
	{
		logger.debug(
			"POST /" + endpoint + "\n" +
			"Content Length: " + contentLength + " Bytes\n" +
			"Resource Name: " + endpoint + "\n" +
			"Body: " + scimJson
		);

		ScimResourceService scimResourceService = scimResourceServices.get(endpoint);

		if (scimResourceService == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return returnExceptionsAsScimErrors(
			()->{
				checkContentLength(contentLength);

				return ResponseEntity.status(200)
					.headers(scimJsonHttpHeader)
					.body(
						scimResourceService.postResource(scimJson)
					);
			}
		);
	}

	/**
	 * @param contentLength The length of the content in bytes
	 * @param endpoint      The endpoint of the resource that is to be searched, described by the ResourceTypes
	 *                      endpoint
	 * @param searchRequest the <code>SearchRequest</code> representing the search criteria.
	 * @return A <code>ListResponse</code> containing the resources matching the search criteria, or an appropriate
	 * 	SCIM	Error Message
	 */
	@PostMapping(value = "/{endpoint}/.search", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Object> searchResource(
		@RequestHeader("content-length") int contentLength,
		@PathVariable String endpoint, @RequestBody SearchRequest searchRequest
	)
	{
		logger.debug(
			"GET /" + endpoint + "/.search\n" +
			"Content Length: " + contentLength + "\n" +
			"Search Request: " + searchRequest
		);
		logger.trace("Request Body : " + searchRequest);

		if (searchRequest.count != null && searchRequest.count > serviceProviderConfig.filter.maxResults)
		{
			ScimException e = new BadRequestException(BadRequestException.ScimType.TOO_MANY);

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		ScimResourceService scimResourceService = scimResourceServices.get(endpoint);

		if (scimResourceService == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return returnExceptionsAsScimErrors(
			()->{
				checkContentLength(contentLength);

				return ResponseEntity.status(200)
					.headers(scimJsonHttpHeader)
					.body(scimResourceService.searchResource(searchRequest));
			}
		);
	}

	/**
	 * @param contentLength the length of the content in bytes
	 * @param endpoint      The endpoint of the resource that is to be modified, described by the ResourceTypes
	 *                      endpoint
	 * @param id            the id of the resource
	 * @param scimJson      the SCIM+JSON representation of the resource to be used to replace the current resource
	 * @return the modified resource
	 */
	@PutMapping(value = "/{endpoint}/{id}", consumes = "application/json")
	@ResponseBody
	// Proper Response is to return the replaced resourceName
	public ResponseEntity<Object> putResource(
		@RequestHeader("content-length") int contentLength,
		@PathVariable String endpoint, @PathVariable String id, @RequestBody String scimJson
	)
	{
		ScimResourceService scimResourceService = scimResourceServices.get(endpoint);

		if (scimResourceService == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return returnExceptionsAsScimErrors(
			()->{
				checkContentLength(contentLength);

				String resource = scimResourceService.putResource(id, scimJson);

				return ResponseEntity.status(200)
					.headers(scimJsonHttpHeader)
					.body(resource);
			}
		);
	}

	/**
	 * @param contentLength the length of the content in bytes
	 * @param endpoint      The endpoint of the resource that is to be patched, described by the ResourceTypes endpoint
	 * @param id            The id of the resource to be modified
	 * @param patchOp       A SCIM PatchOp in JSON format
	 * @return the modified resource
	 */
	@PatchMapping(value = "/{endpoint}/{id}", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Object> patchMapping(
		@RequestHeader("content-length") int contentLength,
		@PathVariable String endpoint, @PathVariable String id, @RequestBody String patchOp
	)
	{
		ScimResourceService scimResourceService = scimResourceServices.get(endpoint);

		if (scimResourceService == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return returnExceptionsAsScimErrors(
			()->{
				checkContentLength(contentLength);

				String resource = scimResourceService.patchResource(
					id,
					objectMapper.readValue(patchOp, new TypeReference<>() {})
				);

				return ResponseEntity.status(200)
					.headers(scimJsonHttpHeader)
					.body(resource);
			}
		);
	}

	/**
	 * if the resource does not exist, return a SCIM+JSON 404 (Not Found) error message if the operation was successful
	 * return the HTTP Status code for No Content, "204" if the operation was not successful, return a SCIM+JSON error
	 * message
	 *
	 * @param endpoint The endpoint of the resource that is to be deleted, described by the ResourceTypes endpoint
	 * @param id       the id of the resource to be deleted
	 * @return HTTP Status 204 if successful
	 */
	@DeleteMapping("/{endpoint}/{id}")
	@ResponseBody
	public ResponseEntity<Object> deleteResource(@PathVariable String endpoint, @PathVariable String id)
	{
		ScimResourceService scimResourceService = scimResourceServices.get(endpoint);

		if (scimResourceService == null)
		{
			ScimException e = new NotFoundException();

			return ResponseEntity.status(e.getHttpStatus())
				.headers(scimJsonHttpHeader)
				.body(e);
		}

		return returnExceptionsAsScimErrors(
			()->{
				scimResourceService.deleteResource(id);

				return ResponseEntity.status(204)
					.headers(scimJsonHttpHeader)
					.body(null);
			}
		);
	}
}
