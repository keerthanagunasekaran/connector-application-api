package com.sath.idhub.connector.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;

/**
 * A Connector Application. A connector application gathers ScimResourceServices and creates a scim interface to access
 * the resources provided by the services (a connected system).
 */
@SpringBootApplication(exclude = {JmxAutoConfiguration.class}, scanBasePackages = "com.sath.idhub")
public class ConnectorApplication
{
	/**
	 * @param args The arguments for this application
	 */
	public static void main(String[] args)
	{
		SpringApplication.run(ConnectorApplication.class, args);
	}
}

