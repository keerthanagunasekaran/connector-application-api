package com.sath.idhub.connector.application.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * The Web Security Configuration for this Connector Application
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter
{
   private final ApplicationContext applicationContext;

   /**
    * @param applicationContext The application context
    */
   public WebSecurityConfiguration(@Autowired ApplicationContext applicationContext)
   {
      this.applicationContext = applicationContext;
   }

   @Override
   protected void configure(HttpSecurity httpSecurity)
      throws Exception
   {
      oAuth(httpSecurity);
   }

   /**
    * @return a new <code>ScimAccessDeniedHandler</code>
    */
   @Bean
   public AccessDeniedHandler getAccessDeniedHandler()
   {
      return new ScimAccessDeniedHandler();
   }

   /**
    * @return a new <code>ScimAuthenticationFailureHandler</code>
    */
   @Bean
   public AuthenticationFailureHandler getAuthenticationFailureHandler()
   {
      return new ScimAuthenticationFailureHandler();
   }

   private void oAuth(HttpSecurity httpSecurity)
       throws Exception
   {
      httpSecurity.authorizeRequests().anyRequest().authenticated();

      httpSecurity.oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt);

      httpSecurity.oauth2ResourceServer().accessDeniedHandler(getAccessDeniedHandler());
   }

   private void noSecurity(HttpSecurity httpSecurity)
       throws Exception
   {
      httpSecurity.authorizeRequests().antMatchers("/**").permitAll()
          .anyRequest().authenticated().and().csrf().disable();
   }
}
