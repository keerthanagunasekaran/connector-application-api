package com.sath.idhub.connector.application.configuration;

import com.sath.idhub.connector.exceptions.scim.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Authentication Failure Handler
 */
public class ScimAuthenticationFailureHandler implements AuthenticationFailureHandler
{
   @Override
   public void onAuthenticationFailure(
      HttpServletRequest request, HttpServletResponse response, AuthenticationException exception
   )
      throws IOException
   {

      response.setStatus(HttpStatus.UNAUTHORIZED.value());

      response.getOutputStream().println(new UnauthorizedException().toString());
   }
}
