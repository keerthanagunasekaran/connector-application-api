package com.sath.idhub.connector.application.configuration;

import com.sath.idhub.connector.exceptions.scim.ForbiddenException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Access Denied Handled
 */
public class ScimAccessDeniedHandler implements AccessDeniedHandler
{
   /**
    * Handles an access denied failure.
    *
    * @param request               that resulted in an <code>AccessDeniedException</code>
    * @param response              so that the user agent can be advised of the failure
    * @param accessDeniedException that caused the invocation
    * @throws IOException      in the event of an IOException
    */
   @Override
   public void handle(
      HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException
   )
      throws IOException
   {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();

      response.getOutputStream().println(new ForbiddenException().toString());
   }
}